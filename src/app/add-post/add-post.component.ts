import { PostService } from './../post.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  title:string;
  author:string;
  body:string; 
  id:string;
  isEdit:boolean = false;
  buttonText:string = 'Add post' 
  postservice: any;

  constructor(private service:PostService, private router:Router,  private route: ActivatedRoute) { }

  onSubmit(){
    if (this.id) {
      this.service.updatePost(this.id,this.title,this.author,this.body);
    }else{
      this.service.addPosts(this.body,this.title,this.author);
    }
    this.router.navigate(['/posts']);
  }


  ngOnInit() {
    this.id = this.route.snapshot.params.id;
      
    if (this.id) {
      this.isEdit = true; 
      this.buttonText = "Updaate book"
      this.service.getpost(this.id).subscribe(
        post=>{
          this.title = post.data().name;
          this.author = post.data().author;
          this.body = post.data().body;
        }
      )
    }
  }

}
