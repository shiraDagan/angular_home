import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
  authors:any = [{id:1,name:'Lewis Carrol'},{id:2,name:'Leo Tolstoy'}, {id:3, name:'Thomas Mann'}];//ניתן את השם והמזהה דרך הסרביס 
  i:number =3;

  
   getAuthors(){
    const authorsObservable = new Observable( //יצירת אובסרבבל
      observer => {
        setInterval(
         ()=> observer.next(this.authors),4000//כל 4 שניות אקבל את רשימת הספרים
        )
      }
    )
    return authorsObservable;
  }


  // addAuthors(){//when we start addbooks, the function will add a new book to the books list, the setniterval sends the list evey 5 seconds.
  //   setInterval(//פונקצייה שטוענת כל פעם את הערך שנרצה שהיא תציג
  //     () => this.authors.push({ name:'A new author'})
  //     ,5000)
  // }
  
  addAuthors(newName){//קבלת הערך החדש שהוזן
      this.i=this.i+1;//הוספת id 
      this.authors.push({id:this.i,name:newName});//הצגת השם והמזהה החדשים של הסופר שהזנו בדף
     
  }
  editAuthors(id,name){//
    this.authors.find(item => item['id'] == id)['name'] = name;//מחפש את המזהה של העורך ומבצע השמה לשם
  }

  constructor() { }
}
