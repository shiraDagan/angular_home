import { Component, OnInit } from '@angular/core';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { ActivatedRoute } from '@angular/router';
import { AuthorsService } from '../authors.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})



export class AuthorsComponent implements OnInit {

  countChecks: number;
  currentId;
  CurrentName;

  panelOpenState = false;
  authors:any;//ההוספה לבוקס אומרת שהוא יכול לקבל כל ערך. 
  authors$:Observable<any>;

  constructor(private route:ActivatedRoute,private authorsservice:AuthorsService ) { }//אתחול משתנים נעשה דרך הקונסטרקטור. ארצה להשתמש באובסרוובל ולכן אאתחל אותו בקובץ זה

  ngOnInit() {
    this.countChecks= 0;
    this.authors$ = this.authorsservice.getAuthors(); //כאן אני קוראת לפונקציה המוגדרת בסרביס
    this.currentId = this.route.snapshot.paramMap.get("id");
    this.CurrentName = this.route.snapshot.paramMap.get("name");
    if(this.currentId!=null) // אם קיים מזהה אז תערוך אותו בסרוויס
    this.authorsservice.editAuthors(this.currentId,this.CurrentName);
  }


  //לאחר לחיצה על הכפתור להוספת עורך חדש 
  onSubmit(form) {
    var name = form.value.authorName;//השם של העורך שהזנו הכנס למשתנה 
    if(name != "")//אם במשתנה יש ערך כלומר הזנו עורך חדש
    this.authorsservice.addAuthors(name);//הערך יישלח לפונקציה שבסרביס
  } 

  toggle(event)
{
  if(event.checked)
  this.countChecks ++;
  else
  this.countChecks --;
}



}
