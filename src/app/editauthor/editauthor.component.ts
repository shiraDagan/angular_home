import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {
  //  @ViewChild('signupForm', {static: false}) sgForm: NgForm;
  id;
  name;
  onSubmit(form) {
    this.david.navigate(['/authors',this.id,form.value.editName])
  }   
  constructor(private route:ActivatedRoute,public david: Router ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id");
    this.name = this.route.snapshot.paramMap.get("name");
      
  }


}
