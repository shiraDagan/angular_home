import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { Post } from './interfaces/post';
import { User } from './interfaces/user';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  // apiurl = "https://jsonplaceholder.typicode.com/posts "//הכנסת הAPI למשתנה
  // apiurluser = "https://jsonplaceholder.typicode.com/users";
  private itemsCollection: AngularFirestoreCollection<Post>;
  // Posts$: Post[];
  // Users$: User[];

  constructor(private http: HttpClient, private db: AngularFirestore) { //הגדרת משתנה מסוג AngularFirestore
    this.itemsCollection = db.collection<Post>('posts');//קולקששין זה כמו רשימה- ככה הפיירסטור שומר את הנתונים . הוא יוכל לקבל רק נתונים מסוג פוסט והוא נקרא פוסטס
  }

  // getPost(){
  //   return this.http.get<Post[]>(this.apiurl)//הפלט של מה שנקבל מפונקציית גט יהיה מסוג פוסט
  // }//this.apiurl הנתונים שפונקציית גט מקבלת
    
  // getUser(){
  //   return this.http.get<User[]>(this.apiurluser);
  // }
      
  
    

private handleError(res: HttpErrorResponse) {//תמיד כשעושים קריאות משתמשים בפונקציה זו שמנהלת את השגיאות- אומרת מה לעשות איתם
  console.error(res.error);
  return throwError(res.error || 'Server error');
}

// addToFirestore(_body:string, _author:string,_name:string){//נקרא לפונקציה שהגדרנו ב ts
//   const post = {body:_body, author:_author, name:_name};//נגדיר משתנה פוסט מסוג ג'ייסון= אובייקט שיקבל מפתח וערך 
//   this.db.collection('posts').add(post);// נקרא לקולקשיין שנקרא פוסטס ונוסיף לו כל פעם את הגוף, העורך והשם 
// }

addPosts(body:string, author:string,title:string){
  const post = {body:body, author:title, name:author};
  this.db.collection('posts').add(post);
}

getposts():Observable<any[]>{
  var response = this.db.collection('posts').valueChanges({idField:'id'});
  return response;
  
}

getpost(id:string):Observable<any>{
  return this.db.doc(`posts/${id}`).get();
}

updatePost(id:string,title:string,author:string,body:string){
  const post = {name:title,author:author,body:body};
  this.db.doc(`posts/${id}`).update(post).then((val)=>console.log("sucssed to upload "+val));
}
      
deletePost(id:string){
  this.db.doc(`posts/${id}`).delete();
}

 
}
