import { Component, OnInit } from '@angular/core';
import { tap, catchError, map } from 'rxjs/operators';
import { PostService } from '../post.service';
import { Observable } from 'rxjs';
import { Post } from '../interfaces/post';
import { User } from '../interfaces/user';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']})

  export class PostsComponent implements OnInit {
  
  // Posts$: Post[];//כשנקבל את הנתונים מהסרוויס נקלוט אותם לתוך משתנה
  // User$: User[];
  title:string;
  body:string;
  author:string;

  posts:any;
  posts$:Observable<any>;
  
  
  constructor( private service:PostService) {  }

  deletepost(id:string){
    this.service.deletePost(id);
  }
  
 
  ngOnInit() {
  //   this.service.getPost()
  //   .subscribe(data =>this.Posts$ = data );//הדטה שאנחנו מקבלים מהפונקציה גט פוסט ייכנס למשתנה פוסט
 
  //   this.service.getUser()
  //   .subscribe(data =>this.User$ = data );
  // }

  // addPPostsToFirestore(){
  //   for (let index = 0; index < this.Posts$.length; index++) {//לולאה שעוברת על הפוסטים
  //     for (let i = 0; i < this.User$.length; i++) {//לולאה שעוברת על העורכים
  //       if (this.Posts$[index].userId==this.User$[i].id) {//השוואת המזהה של הפוסט והעורך אם כן:
  //         this.title = this.Posts$[index].title;//השמה של הכותרת
  //         this.body = this.Posts$[index].body;
  //         this.author = this.User$[i].name;
  //         console.log(this.body,this.author,this.title)
  //         this.service.addToFirestore(this.body,this.author,this.title); //נשלח לפונקציה שבסרביס את הנתונים של הפוסט
  //       }
  //     }  
  //   }
  //  console.log("The data retention was successful")
  // }
//סבסקרייב היא פונקציה שמחזירה משתנה מסוג אובסרבבול. ברגע שהיא תקבל את המידע מהסרוויס היא תוציא אותו למשתנה שאנו רשמנו כדטה
//דטה הוא משתנה מסוג אובסרבבול


  this.posts$ = this.service.getposts();
  }

}
